########################################################
# Import Libraries #####################################
########################################################
import json                                             # used as a more flexible json parser than pandas
import logging                                          # python logging library
import getpass                                          # # used to get the current logged in username from the OS
import sys                                              # used for command line arguments and other system related commands
import os                                               # used to perform os operations such as copy, folder traversing, etc.
import pyodbc                                           # used to make odbc connection to a sqlserver 
import pandas as pd
from datetime import timedelta, datetime                # date helpers
import argparse                                         # parse arguments 
import glob                                             # used to leverage wildcards in file names
import shutil                                           # used to copy/move files
#######################################################
class Inherit(object):
#######################################################
    
    def get_key(self, key, schema = None):
        if schema is None:
            schema = self.config

        keys = key.split(">")
        while len(keys) > 0:
            element = keys.pop(0)
            if element in schema:
                if isinstance(schema[element], dict) and len(keys) > 0:
                    return self.get_key(">".join(keys), schema[element])
                else:  
                    return schema[element]
        return None

#######################################################
class Configuration(Inherit):
#######################################################

    def __init__(self, arguments):
        self.metric_calendar = pd.DataFrame()
        if os.path.join(sys.path[0], "config.json"):
            with open(os.path.join(sys.path[0], "config.json"), "r") as f: self.config = json.load(f)
        
        # read our log
        self.log = Log(os.path.join(self.get_key("paths>log"), self.get_key("log>source")), 
                    {"Token": "", "UseCase": "", "Optional": "", "MeasurePeriod": "", "ProjectedExecutionDate": "", "Step": "", "Status": "", "OverallStatus": ""})
        self.log.read()

        # initialize global logging
        logging.basicConfig(level=f"{self.get_key('log_level').upper()}",
                    format="%(asctime)s|%(levelname)-10s| %(message)s",
                    datefmt="%m/%d/%Y %H:%M:%S",
                    filename=os.path.join(self.get_key("paths>log"), self.get_key("log>application")),
                    filemode="w")
        
        # get metric calendar
        self.metric_calendar = self.get_calendar()

    def get_calendar(self, metric_window = None):
        # get calendar of metrics 
        if metric_window is None: metric_window = self.get_key('config>metric_window')
        calendar = pd.DataFrame()
        today = pd.to_datetime(self.get_date(), format="%m/%d/%Y")

        metric_calendar_file = os.path.join(self.get_key("paths>config"), self.get_key("config>metric_calendar"))
        if os.path.exists(metric_calendar_file):
            with open(metric_calendar_file, "r") as f: calendar = pd.read_csv(f)
            
        # convert dates into panda datetimes
        calendar["ExecutionDate"] = pd.to_datetime(calendar["ExecutionDate"])
        calendar["ExecutionMonth"] = pd.to_datetime(calendar["ExecutionMonth"])

        return calendar[(calendar["ExecutionDate"] >= today - timedelta(days = metric_window)) & (calendar["ExecutionDate"] <= today + timedelta(days = metric_window))]
        
    def get_metric_calendar(self, metric_id):
        if not self.metric_calendar.empty:
            return self.metric_calendar[(self.metric_calendar["MetricId"].isin([metric_id]))]
    
    def read_use_case_configs(self):
        use_cases = []
        for root, dirs, files in os.walk(self.get_key("paths>use_case")):
            for file in files:
                if file.startswith("config") and file.endswith(".json"):    # only load config .json config files
                    logging.debug(f"Found source config file '{os.path.join(root, file)}")
                    #try:
                    with open(os.path.join(root, file), "r") as f: use_case_config = json.load(f)
                    logging.debug(f"\--> Successfully read '{file}'")
                    use_case = UseCase(use_case_config, os.path.join(root, file))
                    use_cases.append(use_case)
                
        logging.debug(f"Instantiated {len(use_cases)} Use Cases")
        return use_cases
    
    def get_date(self):
        return datetime.strftime(pd.Timestamp("now").floor("d"), "%m/%d/%Y")

    def get_use_case_status(self, token, measure_period):
        # get the last 
        log = self.log.get_log()
        log = log.sort_values("LogDate") # sort on log date to make sure we're getting the last record in our log
        result = log.loc[(log['MeasurePeriod'] == measure_period) & (log['Token'] == token)]["OverallStatus"]

        return result.iloc[-1] if result.size > 0 else None

######################################################
class UseCase(Inherit):
######################################################
    def __init__(self, use_case_config, use_case_config_file_path):
        self.config = use_case_config
        self.use_case = self.get_key("use_case")
        self.use_case_folder = use_case_config_file_path[0:use_case_config_file_path.find("config") - 1]       
        self.next_run = {"measure_period": None, "execution_date": "2099-12-31"}
        self.token = self.get_key("token")

        # build use-case log
        self.log = Log(os.path.join(CONFIG.get_key("paths>log"), f"log.{self.get_key('token')}.csv"), 
                {"Token": self.token, "UseCase": self.use_case, "MetricId": "", "MeasurePeriod": "", "Step": "", "StepDetail": "", "StepData": "", "Status": ""})
        self.log.read()

        self.sources = []
        
        if self.get_key("optional") in [None, 'false', 'no']:        
            self.optional = False
        else:
            self.optional = True 
        
        logging.debug(f"Instantiated new Source object for data source '{self.use_case}'")
        
        # create folders if they don't already exist
        if not os.path.exists(self.use_case_folder + "/Input"):
            os.makedirs(self.use_case_folder + "/Input")
        if not os.path.exists(self.use_case_folder + "/Output"):
            os.makedirs(self.use_case_folder + "/Output")
        
        # run only if use_case is enabled
        if self.get_key("enabled") in [None, 'yes', 'True', 'TRUE']:
            self.build_sources()    # build sources found within use_case

            # check if we've already run and were successful by checking the log for the measure period and overall status = Pass           
            if CONFIG.get_use_case_status(self.token, self.next_run["measure_period"]) in [None, 'Fail', 'N/A'] and not self.next_run["measure_period"] is None:
                print(f"Running Use-Case: {self.use_case}")
                for source in self.sources:
                    is_valid = source.process()   # now process the source
                    if is_valid:
                        source.move_files()
                    else:
                        break

                CONFIG.log.message({"Token": self.token, "UseCase": self.use_case, "Optional": "True" if self.optional else "False", "MeasurePeriod": self.next_run["measure_period"], 
                    "ProjectedExecutionDate": self.next_run["execution_date"], "Step": "Build", "Status": "Pass" if is_valid else "Fail", "OverallStatus": "Pass" if is_valid else "Fail" })
                
        CONFIG.log.write()

    def build_sources(self):
        # Build our list of sources that are associated with this use_case
        sources_config = self.get_key("sources")
       
        if not isinstance(sources_config, list):   # ensure sources is a list
            sources_config = [sources_config]
        
        for source_config in sources_config:
            source = Source(source_config, self.log, self.token)
            
            next_run = source.get_next_run()   
            # if the sources next run date is less than what we've already seen, use it instead
            if datetime.strptime(next_run["execution_date"], "%Y-%m-%d") < datetime.strptime(self.next_run["execution_date"], "%Y-%m-%d"):
                self.next_run = next_run

            self.sources.append(source)
        
        
######################################################
class Source(Inherit):
######################################################
    def __init__(self, source_config, log, token):
        self.config = source_config
        self.token = token
        #self.input_file_name = self.get_key("file_name")
        self.input_files = ""
        self.output_file_name = self.get_key("output_file_name")
        self.next_run = {"measure_period": None, "execution_date": "2099-12-31"}
        self.raus = self.get_key("rau_ids")
        self.owner = {"owner_name" : self.get_key("owner_name"), "owner_email" : self.get_key("owner_email")}
        self.log = log
        
        self.metrics = []
        self.metric_ids = []

        self.input_files = self.get_file_names(os.path.join(sys.path[0], CONFIG.get_key("paths>use_case"), f"source-{self.token}", "Input", self.get_key("file_name")))
        self.build_metrics()
        
        # log the status of the input files
        if self.input_files['valid'] is True:
            self.log.message({"Step": "UseCase: File Check", "StepData": "|".join(self.input_files["file_names"]), "MetricId": "|".join(self.metric_ids),
                            "MeasurePeriod": self.next_run["measure_period"], "StepDetail": "File(s) Exist", "Status": "Pass" })
        else:
            self.log.message({"Step": "UseCase: File Check", "StepData": "|".join(self.input_files["file_names"]), "MetricId": "|".join(self.metric_ids),
                            "MeasurePeriod": self.next_run["measure_period"], "StepDetail": "Missing File(s)", "Status": "Fail" })
        
    def build_metrics(self):
        metrics_config = self.get_key("metric_ids")
        
        if not isinstance(metrics_config, list):   # ensure sources is a list
            metrics_config = [metrics_config]
        
        for metric_id in metrics_config:
            metric = Metric(metric_id)
            
            # determine the earliest run date for all metrics.  This will be the source run date
            if metric.to_run() is True:
                next_run = metric.get_next_run()   
                if datetime.strptime(next_run["execution_date"], "%Y-%m-%d") < datetime.strptime(self.next_run["execution_date"], "%Y-%m-%d"):
                   self.next_run =  next_run

            self.metric_ids.append(metric_id)
            self.metrics.append(metric)
            
    def process(self):        
        # if raus were defined in use-case, check if raus exist in input file
        is_valid = True
        if self.input_files["valid"] is True and not self.raus is None:
            rau = pd.DataFrame(self.raus if isinstance(self.raus, list) else [self.raus], columns=["RauId"])
            df_merged = pd.DataFrame
            for file in self.input_files["file_names"]:
                file_data = pd.DataFrame()    
                if file.lower().endswith((".xls", ".xlsx")):
                    file_data = pd.read_excel(file, engine='openpyxl')
                elif file.lower().endswith((".csv")):
                    file_data = pd.read_csv(file)

                # now merge the two dataframes to see if we're missing an rau from our source
                df_merged = rau.merge(rau['RauId'].isin(file_data['RauId'].astype(str)), how='outer', left_index=True, right_index=True)
                df_merged = df_merged[(df_merged["RauId_y"] == False)]
                
            if df_merged.size > 0:  # this means we're missing an RAU
                self.log.message({"Step": "UseCase: RAU Check", "StepData": "|".join(df_merged["RauId_x"]), "MetricId": "|".join(self.metric_ids),
                        "MeasurePeriod": self.next_run["measure_period"], "StepDetail": "Missing RauId(s) from input file(s)", "Status": "Fail" })
                is_valid = False
            else:
                self.log.message({"Step": "UseCase: RAU Check", "StepData": "|".join(rau["RauId"].values.tolist()), "MetricId": "|".join(self.metric_ids),
                        "MeasurePeriod": self.next_run["measure_period"], "StepDetail": "RauId(s) exist", "Status": "Pass" })
          
        self.log.write()
        return is_valid

    def get_next_run(self):
        return self.next_run

    def get_file_names(self, file):
        is_valid = ""
        files = []
        for file in glob.glob(file):  
            file = file[file.rfind(CONFIG.get_key("paths>use_case")):]
            if os.path.exists(file) and is_valid != False:
                # is this an Excel file?
                if file.lower().endswith((".xls", ".xlsx")):
                    df = pd.read_excel(file, engine='openpyxl')      # may only need 'engine' in linux?
                    is_valid = True if df.size > 0 else False
                    if is_valid: files.append(file)
                # or is this a CSV file?
                elif file.lower().endswith(".csv") and os.path.getsize(file) > 0: 
                    is_valid = True
                    files.append(file)
                else:
                    is_valid = False
            else:
                is_valid = False

        return {"valid": False if is_valid == "" else is_valid, "file_names": files if is_valid else file}
        
    def move_files(self):
        input_file = ''.join(self.input_files["file_names"]) # get the filename as a string
        output_file = input_file[0:input_file.find("Input")]  + "Output/" + self.output_file_name # strip off anything after Input
        
        shutil.copy(input_file, output_file)
        #shutil.move(input_file, output_file)

        

######################################################
class Metric(Inherit):
######################################################
    def __init__(self, metric_id):
        self.metric_id = metric_id
        self.calendar = CONFIG.get_metric_calendar(metric_id)
        self.status = ""
        self.owner = ""
        self.rau_id = []    # list of rau ids associated with each metric
        
    def get_next_run(self):
        if self.calendar.size > 0:
            return {"measure_period": self.calendar["MeasurePeriod"].to_string(index=False).strip(), 
                    "execution_date" : pd.to_datetime(self.calendar["ExecutionDate"]).to_string(index=False)}
        else:
            return None

    def to_run(self):
        return False if self.get_next_run() is None else True
    

######################################################
class Log():
#######################################u773948###############
    def __init__(self, log_file = None, template = None):
        self._log = pd.DataFrame() # holds the entirety of our log
        self._queue = pd.DataFrame() # holds our new entries to append to the log file
        self._log_file = log_file
        self._template = template

        if not template is None and not 'LogDate' in template:
            tmp = {"LogDate":""}
            tmp.update(self._template)
            self._template = tmp

    def get_log(self):
        return self._log
    
    def message(self, msg):
        log = self._template
        log.update(msg)         # merge message with initial log template
        log['LogDate'] = datetime.now().strftime("%m/%d/%Y %H:%M:%S")
        self._log = self._log.append(log, ignore_index=True)
        self._queue = self._queue.append(log, ignore_index=True)

    def read(self):
        if os.path.exists(self._log_file):
            with open(self._log_file, "r") as f: self._log = pd.read_csv(f)
    
    def write(self):
        if self._queue.size > 0: # if there's nothing queued, then nothing to do
            self._queue.to_csv(self._log_file, index=False, mode='a', header=False if os.path.exists(self._log_file) else True, columns=self._template)
            self._queue = pd.DataFrame()  # clear out our queued log

########################################################
# Main Program #########################################
########################################################
def main():
    global CONFIG
    
    parser = argparse.ArgumentParser(
        description='This runs the file prep process for the CIB metric application'
    )

    parser.add_argument('-w', '--window', metavar='window', required=False, help='the execution date day window')
    parser.add_argument('-u', '--use_case', metavar='usecase', required=False, help='run a specific use_case')
    parser.add_argument('-m', '--metric', metavar='metric_id', required=False, help='the specific use_case to run')
    
    args = parser.parse_args()

    CONFIG = Configuration(sys.argv)                    # read application config
    use_cases = CONFIG.read_use_case_configs()          # read all use_case configs

 
    logging.info("********************* Program Start *********************")
    logging.info(f"Running as {getpass.getuser()}")
    
    print("Done")

    logging.info("********************** Program End **********************")
if __name__ == "__main__":
    main()
